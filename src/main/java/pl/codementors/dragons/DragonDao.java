package pl.codementors.dragons;

import pl.codementors.dragons.model.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class DragonDao {

    public Dragon persist(Dragon dragon) {
        EntityManager em = DragonEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(dragon);
        tx.commit();
        em.close();
        return dragon;
    }
    public Dragon merge(Dragon dragon) {
        EntityManager em = DragonEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        dragon = em.merge(dragon);
        tx.commit();
        em.close();
        return dragon;
    }


    public Dragon find(int id) {
        EntityManager em = DragonEntityManagerFactory.createEntityManager();
        Dragon dragon = em.find(Dragon.class, id);
        em.close();
        return dragon;
    }


    public void delete(Dragon dragon) {
        EntityManager em = DragonEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(dragon));
        tx.commit();
        em.close();
    }


    public List<Dragon> findAll() {
        EntityManager em = DragonEntityManagerFactory.createEntityManager();
        List<Dragon> dragons = em.createQuery("select d from Dragon d").getResultList();
        em.close();
        return dragons;
    }


   public List<Dragon> findByLand(String landName) {
        EntityManager em = DragonEntityManagerFactory.createEntityManager();
        Query query = em.createQuery("select d from Dragon d join d.land l where l.name = :requestedLand");
        query.setParameter("requestedLand", landName);
        List<Dragon> dragons = query.getResultList();
        em.close();
        return dragons;
    }

    public List<Dragon> findByColor(String color) {
        EntityManager em = DragonEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Dragon> query = cb.createQuery(Dragon.class);
        Root<Dragon> root = query.from(Dragon.class);
        query.select(root);
        query.where(cb.equal(root.get(Dragon_.color), color));
        List<Dragon> dragons = em.createQuery(query).getResultList();
        em.close();
        return dragons;
    }

    public List<Dragon> findByName(String name) {
        EntityManager em = DragonEntityManagerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Dragon> query = cb.createQuery(Dragon.class);
        Root<Dragon> root = query.from(Dragon.class);
        query.select(root);
        query.where(cb.equal(root.get(Dragon_.name), name));
        List<Dragon> dragons = em.createQuery(query).getResultList();
        em.close();
        return dragons;
    }

    public List<Egg> findEggsHeavierThan(int weight) {
        EntityManager em = DragonEntityManagerFactory.createEntityManager();
        Query query = em.createQuery("select e from Egg e where e.weight > :requestedWeight");
        query.setParameter("requestedWeight", weight);
        List<Egg> eggs = query.getResultList();
        em.close();
        return eggs;
    }
}



