package pl.codementors.dragons.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "land")
public class Land {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "land")
    private Set<Dragon> dragon;

    public Land() {
    }

    public Land(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Dragon> getDragons() {
        return dragon;
    }

    public void setDragons(Set<Dragon> dragons) {
        this.dragon = dragons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Land land = (Land) o;
        return id == land.id &&
                Objects.equals(name, land.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Land{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
