package pl.codementors.dragons.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "egg")
public class Egg {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private int diameter;

    @Column
    private int weight;

    @ManyToOne
    @JoinColumn(name = "dragon_id", referencedColumnName = "id")
    private Dragon dragon;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "egg", fetch = FetchType.EAGER, orphanRemoval = true)
    @PrimaryKeyJoinColumn
    private Embellishment embellishment;

    @Override
    public String toString() {
        return "Egg{" +
                "id=" + id +
                ", diameter=" + diameter +
                ", weight=" + weight +
                ", embellishment=" + embellishment +
                '}';
    }

    public Egg() {
    }

    public Egg(int diameter, int weight, Dragon dragon) {
        this.diameter = diameter;
        this.weight = weight;
        this.dragon = dragon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Dragon getDragon() {
        return dragon;
    }

    public void setDragon(Dragon dragon) {
        this.dragon = dragon;
    }

    public Embellishment getEmbellishment() {
        return embellishment;
    }

    public void setEmbellishment(Embellishment embellishment) {
        this.embellishment = embellishment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Egg egg = (Egg) o;
        return id == egg.id &&
                diameter == egg.diameter &&
                weight == egg.weight &&
                Objects.equals(dragon, egg.dragon) &&
                Objects.equals(embellishment, egg.embellishment);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, diameter, weight, dragon, embellishment);
    }

}

