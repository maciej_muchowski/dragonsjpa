package pl.codementors.dragons.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "dragon")
public class Dragon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private String color;

    @Column
    private int wingspan;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dragon", fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Egg> eggList = new ArrayList<>();

    @ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="dragon_land", joinColumns=@JoinColumn(name="dragon_id"), inverseJoinColumns=@JoinColumn(name="land_id"))
    private Set<Land> land;


    public Dragon() {
    }

    public Dragon(String name, String color, int wingspan) {
        this.name = name;
        this.color = color;
        this.wingspan = wingspan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWingspan() {
        return wingspan;
    }

    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }

    public List<Egg> getEggList() {
        return eggList;
    }

    public void setEggList(List<Egg> eggList) {
        this.eggList = eggList;
    }

    public Set<Land> getLand() {
        return land;
    }

    public void setLand(Set<Land> land) {
        this.land = land;
    }

    @Override
    public String toString() {
        return "Dragon{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", wingspan=" + wingspan +
                ", eggList=" + eggList +
                ", land=" + land +
                '}';
    }
}
