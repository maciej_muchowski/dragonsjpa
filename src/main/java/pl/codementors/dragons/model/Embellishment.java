package pl.codementors.dragons.model;

import javax.persistence.*;

@Entity
@Table(name = "embellishment")
public class Embellishment {

    @Id
    private int id;

    @Column
    private String color;

    @Column
    private String pattern;

    @MapsId
    @JoinColumn(name = "id")
    @OneToOne(cascade = CascadeType.REMOVE)
    private Egg egg;


    public Embellishment() {
    }

    public Embellishment(String color, String pattern, Egg egg) {
        this.color = color;
        this.pattern = pattern;
        this.egg = egg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public String toString() {
        return "Embellishment{" +
                "id=" + id +
                ", color='" + color + '\'' +
                ", pattern='" + pattern + '\'' +
                '}';
    }
}
