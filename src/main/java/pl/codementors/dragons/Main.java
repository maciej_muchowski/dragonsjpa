package pl.codementors.dragons;

import pl.codementors.dragons.model.Dragon;
import pl.codementors.dragons.model.Egg;
import pl.codementors.dragons.model.Embellishment;
import pl.codementors.dragons.model.Land;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        DragonDao dao = new DragonDao();

        Dragon alduin = new Dragon("Alduin", "Black", 50);

        Egg egg1 = new Egg(10, 40, alduin);
        Egg egg2 = new Egg(15, 50, alduin);

        Embellishment red = new Embellishment("red", "mesh", egg1);
        Embellishment yellow = new Embellishment("yellow", "dotted", egg2);

        egg1.setEmbellishment(red);
        egg2.setEmbellishment(yellow);

        List<Egg> eggs = new ArrayList<>();
        eggs.add(egg1);
        eggs.add(egg2);
        alduin.setEggList(eggs);

        Land skyrim = new Land("Skyrim");
        Land solstheim = new Land("Solstheim");

        alduin.setLand(Stream.of(skyrim,solstheim).collect(Collectors.toSet()));

        alduin = dao.persist(alduin);

        dao.findAll().forEach(System.out::println);
        dao.findByColor("Black").forEach(System.out::println);
        dao.findByName("Alduin").forEach(System.out::println);
        dao.findEggsHeavierThan(41).forEach(System.out::println);
        dao.findByLand("Skyrim").forEach(System.out::println);
        dao.delete(alduin);

        DragonEntityManagerFactory.close();
    }
}
