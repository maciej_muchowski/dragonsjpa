CREATE USER 'dragons_user' IDENTIFIED BY 'dr4g0ns';

CREATE DATABASE dragons CHARACTER SET utf8 COLLATE utf8_general_ci;

GRANT ALL ON dragons.* TO 'dragons_user';

USE dragons;

CREATE TABLE dragon (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128),
    color VARCHAR(128), 
    wingspan INT
);
CREATE TABLE egg (
    id INT PRIMARY KEY AUTO_INCREMENT,
    weight INT,
    diameter INT, 
    dragon_id INT
);
ALTER TABLE egg ADD FOREIGN KEY (dragon_id) REFERENCES dragon (id);

CREATE TABLE embellishment (
    id INT PRIMARY KEY,
    color VARCHAR(128),
    pattern VARCHAR(128),
    FOREIGN KEY (id) REFERENCES egg (id)
);
CREATE TABLE land (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL
);
CREATE TABLE dragon_land (
    dragon_id INT NOT NULL,
    land_id INT NOT NULL
);
ALTER TABLE dragon_land ADD PRIMARY KEY (dragon_id, land_id);
ALTER TABLE dragon_land ADD FOREIGN KEY (dragon_id) REFERENCES dragon (id);
ALTER TABLE dragon_land ADD FOREIGN KEY (land_id) REFERENCES land (id);
